#line 2 "../gotwebd/pages.tmpl"
/*
* Copyright (c) 2022 Omar Polo <op@openbsd.org>
* Copyright (c) 2016, 2019, 2020-2022 Tracey Emery <tracey@traceyemery.net>
*
* Permission to use, copy, modify, and distribute this software for any
* purpose with or without fee is hereby granted, provided that the above
* copyright notice and this permission notice appear in all copies.
*
* THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
* WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
* ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
* WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
* ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
* OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/
#line 19 "../gotwebd/pages.tmpl"
#include <sys/types.h>
#include <sys/queue.h>
#include <sys/stat.h>
#line 23 "../gotwebd/pages.tmpl"
#include <ctype.h>
#include <event.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <imsg.h>
#line 31 "../gotwebd/pages.tmpl"
#include "got_error.h"
#include "got_object.h"
#include "got_reference.h"
#line 35 "../gotwebd/pages.tmpl"
#include "proc.h"
#line 37 "../gotwebd/pages.tmpl"
#include "gotwebd.h"
#include "tmpl.h"
#line 40 "../gotwebd/pages.tmpl"
static int gotweb_render_blob_line(struct template *, const char *, size_t);
static int gotweb_render_tree_item(struct template *, struct got_tree_entry *);
static int blame_line(struct template *, const char *, struct blame_line *,
int, int);
#line 45 "../gotwebd/pages.tmpl"
static inline int gotweb_render_more(struct template *, int);
#line 47 "../gotwebd/pages.tmpl"
static inline int diff_line(struct template *, char *);
static inline int tag_item(struct template *, struct repo_tag *);
static inline int branch(struct template *, struct got_reflist_entry *);
static inline int rss_tag_item(struct template *, struct repo_tag *);
static inline int rss_author(struct template *, char *);
#line 55 "../gotwebd/pages.tmpl"
int
gotweb_render_header(struct template *tp)
{
int tp_ret = 0;
#line 57 "../gotwebd/pages.tmpl"
struct request *c = tp->tp_arg;
struct server *srv = c->srv;
struct querystring *qs = c->t->qs;
struct gotweb_url u_path;
const char *prfx = c->document_uri;
const char *css = srv->custom_css;
#line 64 "../gotwebd/pages.tmpl"
memset(&u_path, 0, sizeof(u_path));
u_path.index_page = -1;
u_path.page = -1;
u_path.action = SUMMARY;
#line 69 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "<!doctype html>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<html>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<head>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<meta charset=\"utf-8\" />")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<title>")) == -1) goto err;
#line 73 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_escape(tp, srv->site_name)) == -1)
goto err;
#line 73 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "</title>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<meta name=\"viewport\" content=\"initial-scale=.75\" />")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<meta name=\"msapplication-TileColor\" content=\"#da532c\" />")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<meta name=\"theme-color\" content=\"#ffffff\"/>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<link rel=\"apple-touch-icon\" sizes=\"180x180\" href=\"")) == -1) goto err;
#line 77 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_escape(tp, prfx)) == -1)
goto err;
#line 77 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "apple-touch-icon.png\" />")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<link rel=\"icon\" type=\"image/png\" sizes=\"32x32\" href=\"")) == -1) goto err;
#line 78 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_escape(tp, prfx)) == -1)
goto err;
#line 78 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "favicon-32x32.png\" />")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<link rel=\"icon\" type=\"image/png\" sizes=\"16x16\" href=\"")) == -1) goto err;
#line 79 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_escape(tp, prfx)) == -1)
goto err;
#line 79 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "favicon-16x16.png\" />")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<link rel=\"manifest\" href=\"")) == -1) goto err;
#line 80 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_escape(tp, prfx)) == -1)
goto err;
#line 80 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "site.webmanifest\"/>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<link rel=\"mask-icon\" href=\"")) == -1) goto err;
#line 81 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_escape(tp, prfx)) == -1)
goto err;
#line 81 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "safari-pinned-tab.svg\" />")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<link rel=\"stylesheet\" type=\"text/css\" href=\"")) == -1) goto err;
#line 82 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_escape(tp, prfx)) == -1)
goto err;
#line 82 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_escape(tp, css)) == -1)
goto err;
#line 82 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "\" />")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</head>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<body>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div id=\"gw_body\">")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div id=\"header\">")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div id=\"got_link\">")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<a href=\"")) == -1) goto err;
#line 88 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_escape(tp, srv->logo_url)) == -1)
goto err;
#line 88 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "\" target=\"_blank\">")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<img src=\"")) == -1) goto err;
#line 89 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_escape(tp, prfx)) == -1)
goto err;
#line 89 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_escape(tp, srv->logo)) == -1)
goto err;
#line 89 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "\" />")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</a>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div id=\"site_path\">")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div id=\"site_link\">")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<a href=\"?index_page=")) == -1) goto err;
#line 95 "../gotwebd/pages.tmpl"
if (asprintf(&tp->tp_tmp,  "%d", qs->index_page) == -1)
goto err;
if ((tp_ret = tp->tp_escape(tp, tp->tp_tmp)) == -1)
goto err;
free(tp->tp_tmp);
tp->tp_tmp = NULL;
#line 95 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "\">")) == -1) goto err;
if ((tp_ret = tp->tp_escape(tp, srv->site_link)) == -1)
goto err;
if ((tp_ret = tp->tp_puts(tp, "</a>")) == -1) goto err;
if (qs->path) {
u_path.path = qs->path; 
if ((tp_ret = tp->tp_escape(tp, " / ")) == -1)
goto err;
if ((tp_ret = tp->tp_puts(tp, "<a href=\"")) == -1) goto err;
#line 101 "../gotwebd/pages.tmpl"
if ((tp_ret = gotweb_render_url(tp->tp_arg, &u_path)) == -1) goto err;
#line 101 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "\">")) == -1) goto err;
if ((tp_ret = tp->tp_escape(tp, qs->path)) == -1)
goto err;
if ((tp_ret = tp->tp_puts(tp, "</a>")) == -1) goto err;
}
#line 105 "../gotwebd/pages.tmpl"
if (qs->action != INDEX) {
if ((tp_ret = tp->tp_escape(tp, " / ")) == -1)
goto err;
#line 106 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_escape(tp, gotweb_action_name(qs->action))) == -1)
goto err;
}
#line 108 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div id=\"content\">")) == -1) goto err;
err:
return tp_ret;
}
#line 113 "../gotwebd/pages.tmpl"
int
gotweb_render_footer(struct template *tp)
{
int tp_ret = 0;
#line 115 "../gotwebd/pages.tmpl"
struct request *c = tp->tp_arg;
struct server *srv = c->srv;
#line 118 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "<div id=\"site_owner_wrapper\">")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div id=\"site_owner\">")) == -1) goto err;
if (srv->show_site_owner) {
if ((tp_ret = tp->tp_escape(tp, srv->site_owner)) == -1)
goto err;
}
#line 123 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</body>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</html>")) == -1) goto err;
err:
return tp_ret;
}
#line 131 "../gotwebd/pages.tmpl"
int
gotweb_render_repo_table_hdr(struct template *tp)
{
int tp_ret = 0;
#line 133 "../gotwebd/pages.tmpl"
struct request *c = tp->tp_arg;
struct server *srv = c->srv;
#line 136 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "<div id=\"index_header\">")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div id=\"index_header_project\">")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "Project")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
if (srv->show_repo_description) {
if ((tp_ret = tp->tp_puts(tp, "<div id=\"index_header_description\">")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "Description")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
}
#line 145 "../gotwebd/pages.tmpl"
if (srv->show_repo_owner) {
if ((tp_ret = tp->tp_puts(tp, "<div id=\"index_header_owner\">")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "Owner")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
}
#line 150 "../gotwebd/pages.tmpl"
if (srv->show_repo_age) {
if ((tp_ret = tp->tp_puts(tp, "<div id=\"index_header_age\">")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "Last Change")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
}
#line 155 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
err:
return tp_ret;
}
#line 158 "../gotwebd/pages.tmpl"
int
gotweb_render_repo_fragment(struct template *tp, struct repo_dir *repo_dir)
{
int tp_ret = 0;
#line 160 "../gotwebd/pages.tmpl"
struct request *c = tp->tp_arg;
struct server *srv = c->srv;
struct gotweb_url summary = {
.action = SUMMARY,
.index_page = -1,
.page = -1,
.path = repo_dir->name,
}, briefs = {
.action = BRIEFS,
.index_page = -1,
.page = -1,
.path = repo_dir->name,
}, commits = {
.action = COMMITS,
.index_page = -1,
.page = -1,
.path = repo_dir->name,
}, tags = {
.action = TAGS,
.index_page = -1,
.page = -1,
.path = repo_dir->name,
}, tree = {
.action = TREE,
.index_page = -1,
.page = -1,
.path = repo_dir->name,
}, rss = {
.action = RSS,
.index_page = -1,
.page = -1,
.path = repo_dir->name,
};
#line 194 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "<div class=\"index_wrapper\">")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div class=\"index_project\">")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<a href=\"")) == -1) goto err;
#line 196 "../gotwebd/pages.tmpl"
if ((tp_ret = gotweb_render_url(tp->tp_arg, &summary)) == -1) goto err;
#line 196 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "\">")) == -1) goto err;
#line 196 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_escape(tp, repo_dir->name)) == -1)
goto err;
#line 196 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "</a>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
if (srv->show_repo_description) {
if ((tp_ret = tp->tp_puts(tp, "<div class=\"index_project_description\">")) == -1) goto err;
if ((tp_ret = tp->tp_escape(tp, repo_dir->description)) == -1)
goto err;
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
}
#line 203 "../gotwebd/pages.tmpl"
if (srv->show_repo_owner) {
if ((tp_ret = tp->tp_puts(tp, "<div class=\"index_project_owner\">")) == -1) goto err;
if ((tp_ret = tp->tp_escape(tp, repo_dir->owner)) == -1)
goto err;
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
}
#line 208 "../gotwebd/pages.tmpl"
if (srv->show_repo_age) {
if ((tp_ret = tp->tp_puts(tp, "<div class=\"index_project_age\">")) == -1) goto err;
if ((tp_ret = gotweb_render_age(tp, repo_dir->age, TM_DIFF)) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
}
#line 213 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "<div class=\"navs_wrapper\">")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div class=\"navs\">")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<a href=\"")) == -1) goto err;
#line 215 "../gotwebd/pages.tmpl"
if ((tp_ret = gotweb_render_url(tp->tp_arg, &summary)) == -1) goto err;
#line 215 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "\">summary</a>")) == -1) goto err;
if ((tp_ret = tp->tp_escape(tp, " | ")) == -1)
goto err;
if ((tp_ret = tp->tp_puts(tp, "<a href=\"")) == -1) goto err;
#line 217 "../gotwebd/pages.tmpl"
if ((tp_ret = gotweb_render_url(tp->tp_arg, &briefs)) == -1) goto err;
#line 217 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "\">briefs</a>")) == -1) goto err;
if ((tp_ret = tp->tp_escape(tp, " | ")) == -1)
goto err;
if ((tp_ret = tp->tp_puts(tp, "<a href=\"")) == -1) goto err;
#line 219 "../gotwebd/pages.tmpl"
if ((tp_ret = gotweb_render_url(tp->tp_arg, &commits)) == -1) goto err;
#line 219 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "\">commits</a>")) == -1) goto err;
if ((tp_ret = tp->tp_escape(tp, " | ")) == -1)
goto err;
if ((tp_ret = tp->tp_puts(tp, "<a href=\"")) == -1) goto err;
#line 221 "../gotwebd/pages.tmpl"
if ((tp_ret = gotweb_render_url(tp->tp_arg, &tags)) == -1) goto err;
#line 221 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "\">tags</a>")) == -1) goto err;
if ((tp_ret = tp->tp_escape(tp, " | ")) == -1)
goto err;
if ((tp_ret = tp->tp_puts(tp, "<a href=\"")) == -1) goto err;
#line 223 "../gotwebd/pages.tmpl"
if ((tp_ret = gotweb_render_url(tp->tp_arg, &tree)) == -1) goto err;
#line 223 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "\">tree</a>")) == -1) goto err;
if ((tp_ret = tp->tp_escape(tp, " | ")) == -1)
goto err;
if ((tp_ret = tp->tp_puts(tp, "<a href=\"")) == -1) goto err;
#line 225 "../gotwebd/pages.tmpl"
if ((tp_ret = gotweb_render_url(tp->tp_arg, &rss)) == -1) goto err;
#line 225 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "\">rss</a>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div class=\"dotted_line\"></div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
err:
return tp_ret;
}
#line 232 "../gotwebd/pages.tmpl"
int
gotweb_render_briefs(struct template *tp)
{
int tp_ret = 0;
#line 234 "../gotwebd/pages.tmpl"
const struct got_error *error;
struct request *c = tp->tp_arg;
struct server *srv = c->srv;
struct transport *t = c->t;
struct querystring *qs = c->t->qs;
struct repo_commit *rc;
struct repo_dir *repo_dir = t->repo_dir;
struct gotweb_url diff_url, tree_url;
char *tmp;
#line 244 "../gotwebd/pages.tmpl"
diff_url = (struct gotweb_url){
.action = DIFF,
.index_page = -1,
.page = -1,
.path = repo_dir->name,
.headref = qs->headref,
};
tree_url = (struct gotweb_url){
.action = TREE,
.index_page = -1,
.page = -1,
.path = repo_dir->name,
.headref = qs->headref,
};
#line 259 "../gotwebd/pages.tmpl"
if (qs->action == SUMMARY) {
qs->action = BRIEFS;
error = got_get_repo_commits(c, D_MAXSLCOMMDISP);
} else
error = got_get_repo_commits(c, srv->max_commits_display);
if (error)
return -1;
#line 267 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "<div id=\"briefs_title_wrapper\">")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div id=\"briefs_title\">Commit Briefs</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div id=\"briefs_content\">")) == -1) goto err;
TAILQ_FOREACH(rc, &t->repo_commits, entry) {
#line 273 "../gotwebd/pages.tmpl"
diff_url.commit = rc->commit_id;
tree_url.commit = rc->commit_id;
#line 276 "../gotwebd/pages.tmpl"
tmp = strchr(rc->committer, '<');
if (tmp)
*tmp = '\0';
#line 280 "../gotwebd/pages.tmpl"
tmp = strchr(rc->commit_msg, '\n');
if (tmp)
*tmp = '\0';
#line 284 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "<div class=\"briefs_age\">")) == -1) goto err;
if ((tp_ret = gotweb_render_age(tp, rc->committer_time, TM_DIFF)) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div class=\"briefs_author\">")) == -1) goto err;
if ((tp_ret = tp->tp_escape(tp, rc->committer)) == -1)
goto err;
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div class=\"briefs_log\">")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<a href=\"")) == -1) goto err;
#line 291 "../gotwebd/pages.tmpl"
if ((tp_ret = gotweb_render_url(tp->tp_arg, &diff_url)) == -1) goto err;
#line 291 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "\">")) == -1) goto err;
if ((tp_ret = tp->tp_escape(tp, rc->commit_msg)) == -1)
goto err;
if ((tp_ret = tp->tp_puts(tp, "</a>")) == -1) goto err;
if (rc->refs_str) {
if ((tp_ret = tp->tp_escape(tp, " ")) == -1)
goto err;
#line 295 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "<span class=\"refs_str\">(")) == -1) goto err;
#line 295 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_escape(tp, rc->refs_str)) == -1)
goto err;
#line 295 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, ")</span>")) == -1) goto err;
}
#line 297 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "</a>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div class=\"navs_wrapper\">")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div class=\"navs\">")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<a href=\"")) == -1) goto err;
#line 301 "../gotwebd/pages.tmpl"
if ((tp_ret = gotweb_render_url(tp->tp_arg, &diff_url)) == -1) goto err;
#line 301 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "\">diff</a>")) == -1) goto err;
if ((tp_ret = tp->tp_escape(tp, " | ")) == -1)
goto err;
if ((tp_ret = tp->tp_puts(tp, "<a href=\"")) == -1) goto err;
#line 303 "../gotwebd/pages.tmpl"
if ((tp_ret = gotweb_render_url(tp->tp_arg, &tree_url)) == -1) goto err;
#line 303 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "\">tree</a>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div class=\"dotted_line\"></div>")) == -1) goto err;
}
#line 308 "../gotwebd/pages.tmpl"
if ((tp_ret = gotweb_render_more(tp, BRIEFS)) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
err:
return tp_ret;
}
#line 312 "../gotwebd/pages.tmpl"
int
gotweb_render_more(struct template *tp, int action)
{
int tp_ret = 0;
#line 314 "../gotwebd/pages.tmpl"
struct request *c = tp->tp_arg;
struct transport *t = c->t;
struct querystring *qs = t->qs;
struct gotweb_url more = {
.action = action,
.index_page = -1,
.path = qs->path,
.commit = t->more_id,
.headref = qs->headref,
};
#line 325 "../gotwebd/pages.tmpl"
if (t->more_id) {
if ((tp_ret = tp->tp_puts(tp, "<div id=\"np_wrapper\">")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div id=\"nav_more\">")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<a href=\"")) == -1) goto err;
#line 328 "../gotwebd/pages.tmpl"
if ((tp_ret = gotweb_render_url(c, &more)) == -1) goto err;
#line 328 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "\">")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "More&nbsp;&darr;")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</a>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
}
err:
return tp_ret;
}
#line 336 "../gotwebd/pages.tmpl"
int
gotweb_render_navs(struct template *tp)
{
int tp_ret = 0;
#line 338 "../gotwebd/pages.tmpl"
struct request *c = tp->tp_arg;
struct transport *t = c->t;
struct gotweb_url prev, next;
int have_prev, have_next;
#line 343 "../gotwebd/pages.tmpl"
gotweb_get_navs(c, &prev, &have_prev, &next, &have_next);
#line 345 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "<div id=\"np_wrapper\">")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div id=\"nav_prev\">")) == -1) goto err;
if (have_prev) {
if ((tp_ret = tp->tp_puts(tp, "<a href=\"")) == -1) goto err;
#line 348 "../gotwebd/pages.tmpl"
if ((tp_ret = gotweb_render_url(c, &prev)) == -1) goto err;
#line 348 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "\">")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "Previous")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</a>")) == -1) goto err;
}
#line 352 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div id=\"nav_next\">")) == -1) goto err;
if (have_next) {
if ((tp_ret = tp->tp_puts(tp, "<a href=\"")) == -1) goto err;
#line 355 "../gotwebd/pages.tmpl"
if ((tp_ret = gotweb_render_url(c, &next)) == -1) goto err;
#line 355 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "\">")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "Next")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</a>")) == -1) goto err;
}
#line 359 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
err:
#line 363 "../gotwebd/pages.tmpl"
free(t->next_id);
t->next_id = NULL;
free(t->prev_id);
t->prev_id = NULL;
return tp_ret;
}
#line 370 "../gotwebd/pages.tmpl"
int
gotweb_render_commits(struct template *tp)
{
int tp_ret = 0;
#line 372 "../gotwebd/pages.tmpl"
struct request *c = tp->tp_arg;
struct transport *t = c->t;
struct repo_dir *repo_dir = t->repo_dir;
struct repo_commit *rc;
struct gotweb_url diff, tree;
#line 378 "../gotwebd/pages.tmpl"
diff = (struct gotweb_url){
.action = DIFF,
.index_page = -1,
.page = -1,
.path = repo_dir->name,
};
tree = (struct gotweb_url){
.action = TREE,
.index_page = -1,
.page = -1,
.path = repo_dir->name,
};
#line 391 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "<div class=\"commits_title_wrapper\">")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div class=\"commits_title\">Commits</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div class=\"commits_content\">")) == -1) goto err;
TAILQ_FOREACH(rc, &t->repo_commits, entry) {
#line 397 "../gotwebd/pages.tmpl"
diff.commit = rc->commit_id;
tree.commit = rc->commit_id;
#line 400 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "<div class=\"commits_header_wrapper\">")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div class=\"commits_header\">")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div class=\"header_commit_title\">Commit:</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div class=\"header_commit\">")) == -1) goto err;
#line 403 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_escape(tp, rc->commit_id)) == -1)
goto err;
#line 403 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div class=\"header_author_title\">From:</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div class=\"header_author\">")) == -1) goto err;
#line 405 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_escape(tp, rc->author)) == -1)
goto err;
#line 405 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
if (strcmp(rc->committer, rc->author) != 0) {
if ((tp_ret = tp->tp_puts(tp, "<div class=\"header_author_title\">Via:</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div class=\"header_author\">")) == -1) goto err;
#line 408 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_escape(tp, rc->committer)) == -1)
goto err;
#line 408 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
}
#line 410 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "<div class=\"header_age_title\">Date:</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div class=\"header_age\">")) == -1) goto err;
if ((tp_ret = gotweb_render_age(tp, rc->committer_time, TM_LONG)) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div class=\"dotted_line\"></div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div class=\"commit\">")) == -1) goto err;
if ((tp_ret = tp->tp_escape(tp, "\n")) == -1)
goto err;
if ((tp_ret = tp->tp_escape(tp, rc->commit_msg)) == -1)
goto err;
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div class=\"navs_wrapper\">")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div class=\"navs\">")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<a href=\"")) == -1) goto err;
#line 423 "../gotwebd/pages.tmpl"
if ((tp_ret = gotweb_render_url(c, &diff)) == -1) goto err;
#line 423 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "\">diff</a>")) == -1) goto err;
if ((tp_ret = tp->tp_escape(tp, " | ")) == -1)
goto err;
if ((tp_ret = tp->tp_puts(tp, "<a href=\"")) == -1) goto err;
#line 425 "../gotwebd/pages.tmpl"
if ((tp_ret = gotweb_render_url(c, &tree)) == -1) goto err;
#line 425 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "\">tree</a>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div class=\"dotted_line\"></div>")) == -1) goto err;
}
#line 430 "../gotwebd/pages.tmpl"
if ((tp_ret = gotweb_render_more(tp, COMMITS)) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
err:
return tp_ret;
}
#line 435 "../gotwebd/pages.tmpl"
int
gotweb_render_blob(struct template *tp, struct got_blob_object *blob)
{
int tp_ret = 0;
#line 437 "../gotwebd/pages.tmpl"
struct request *c = tp->tp_arg;
struct transport *t = c->t;
struct repo_commit *rc = TAILQ_FIRST(&t->repo_commits);
#line 441 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "<div id=\"blob_title_wrapper\">")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div id=\"blob_title\">Blob</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div id=\"blob_content\">")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div id=\"blob_header_wrapper\">")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div id=\"blob_header\">")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div class=\"header_age_title\">Date:</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div class=\"header_age\">")) == -1) goto err;
if ((tp_ret = gotweb_render_age(tp, rc->committer_time, TM_LONG)) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div id=\"header_commit_msg_title\">Message:</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div id=\"header_commit_msg\">")) == -1) goto err;
#line 452 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_escape(tp, rc->commit_msg)) == -1)
goto err;
#line 452 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div class=\"dotted_line\"></div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div id=\"blob\">")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<pre>")) == -1) goto err;
if ((tp_ret = got_output_blob_by_lines(tp, blob, gotweb_render_blob_line)) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</pre>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
err:
return tp_ret;
}
#line 465 "../gotwebd/pages.tmpl"
int
gotweb_render_blob_line(struct template *tp, const char *line, size_t no)
{
int tp_ret = 0;
#line 467 "../gotwebd/pages.tmpl"
char lineno[16];
int r;
#line 470 "../gotwebd/pages.tmpl"
r = snprintf(lineno, sizeof(lineno), "%zu", no);
if (r < 0 || (size_t)r >= sizeof(lineno))
return -1;
#line 474 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "<div class=\"blob_line\" id=\"line")) == -1) goto err;
#line 474 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_escape(tp, lineno)) == -1)
goto err;
#line 474 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "\">")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div class=\"blob_number\">")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<a href=\"#line")) == -1) goto err;
#line 476 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_escape(tp, lineno)) == -1)
goto err;
#line 476 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "\">")) == -1) goto err;
#line 476 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_escape(tp, lineno)) == -1)
goto err;
#line 476 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "</a>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div class=\"blob_code\">")) == -1) goto err;
#line 478 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_escape(tp, line)) == -1)
goto err;
#line 478 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
err:
return tp_ret;
}
#line 482 "../gotwebd/pages.tmpl"
int
gotweb_render_tree(struct template *tp)
{
int tp_ret = 0;
#line 484 "../gotwebd/pages.tmpl"
struct request *c = tp->tp_arg;
struct transport *t = c->t;
struct repo_commit *rc = TAILQ_FIRST(&t->repo_commits);
#line 488 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "<div id=\"tree_title_wrapper\">")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div id=\"tree_title\">Tree</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div id=\"tree_content\">")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div id=\"tree_header_wrapper\">")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div id=\"tree_header\">")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div id=\"header_tree_title\">Tree:</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div id=\"header_tree\">")) == -1) goto err;
#line 495 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_escape(tp, rc->tree_id)) == -1)
goto err;
#line 495 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div class=\"header_age_title\">Date:</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div class=\"header_age\">")) == -1) goto err;
if ((tp_ret = gotweb_render_age(tp, rc->committer_time, TM_LONG)) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div id=\"header_commit_msg_title\">Message:</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div id=\"header_commit_msg\">")) == -1) goto err;
#line 501 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_escape(tp, rc->commit_msg)) == -1)
goto err;
#line 501 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div class=\"dotted_line\"></div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div id=\"tree\">")) == -1) goto err;
if ((tp_ret = got_output_repo_tree(c, gotweb_render_tree_item)) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
err:
return tp_ret;
}
#line 512 "../gotwebd/pages.tmpl"
int
gotweb_render_tree_item(struct template *tp, struct got_tree_entry *te)
{
int tp_ret = 0;
#line 514 "../gotwebd/pages.tmpl"
struct request *c = tp->tp_arg;
struct transport *t = c->t;
struct querystring *qs = t->qs;
struct repo_commit *rc = TAILQ_FIRST(&t->repo_commits);
const char *modestr = "";
const char *name;
const char *folder;
char *dir = NULL;
mode_t mode;
struct gotweb_url url = {
.index_page = -1,
.page = -1,
.commit = rc->commit_id,
.path = qs->path,
};
#line 530 "../gotwebd/pages.tmpl"
name = got_tree_entry_get_name(te);
mode = got_tree_entry_get_mode(te);
#line 533 "../gotwebd/pages.tmpl"
folder = qs->folder ? qs->folder : "";
if (S_ISDIR(mode)) {
if (asprintf(&dir, "%s/%s", folder, name) == -1)
return (-1);
#line 538 "../gotwebd/pages.tmpl"
url.action = TREE;
url.folder = dir;
} else {
url.action = BLOB;
url.folder = folder;
url.file = name;
}
#line 546 "../gotwebd/pages.tmpl"
if (got_object_tree_entry_is_submodule(te))
modestr = "$";
else if (S_ISLNK(mode))
modestr = "@";
else if (S_ISDIR(mode))
modestr = "/";
else if (mode & S_IXUSR)
modestr = "*";
#line 555 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "<div class=\"tree_wrapper\">")) == -1) goto err;
if (S_ISDIR(mode)) {
if ((tp_ret = tp->tp_puts(tp, "<div class=\"tree_line\">")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<a href=\"")) == -1) goto err;
#line 558 "../gotwebd/pages.tmpl"
if ((tp_ret = gotweb_render_url(c, &url)) == -1) goto err;
#line 558 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "\">")) == -1) goto err;
if ((tp_ret = tp->tp_escape(tp, name)) == -1)
goto err;
#line 559 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_escape(tp, modestr)) == -1)
goto err;
if ((tp_ret = tp->tp_puts(tp, "</a>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div class=\"tree_line_blank\">&nbsp;</div>")) == -1) goto err;
} else {
if ((tp_ret = tp->tp_puts(tp, "<div class=\"tree_line\">")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<a href=\"")) == -1) goto err;
#line 565 "../gotwebd/pages.tmpl"
if ((tp_ret = gotweb_render_url(c, &url)) == -1) goto err;
#line 565 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "\">")) == -1) goto err;
if ((tp_ret = tp->tp_escape(tp, name)) == -1)
goto err;
#line 566 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_escape(tp, modestr)) == -1)
goto err;
if ((tp_ret = tp->tp_puts(tp, "</a>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div class=\"tree_line_blank\">")) == -1) goto err;
url.action = COMMITS; 
if ((tp_ret = tp->tp_puts(tp, "<a href=\"")) == -1) goto err;
#line 571 "../gotwebd/pages.tmpl"
if ((tp_ret = gotweb_render_url(c, &url)) == -1) goto err;
#line 571 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "\">")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "commits")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</a>")) == -1) goto err;
if ((tp_ret = tp->tp_escape(tp, " | ")) == -1)
goto err;
url.action = BLAME; 
if ((tp_ret = tp->tp_puts(tp, "<a href=\"")) == -1) goto err;
#line 576 "../gotwebd/pages.tmpl"
if ((tp_ret = gotweb_render_url(c, &url)) == -1) goto err;
#line 576 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "\">")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "blame")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</a>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
}
#line 581 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
err:
#line 584 "../gotwebd/pages.tmpl"
free(dir);
return tp_ret;
}
#line 588 "../gotwebd/pages.tmpl"
int
gotweb_render_tags(struct template *tp)
{
int tp_ret = 0;
#line 590 "../gotwebd/pages.tmpl"
struct request *c = tp->tp_arg;
struct transport *t = c->t;
struct querystring *qs = t->qs;
struct repo_tag *rt;
int commit_found;
#line 596 "../gotwebd/pages.tmpl"
commit_found = qs->commit == NULL;
#line 598 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "<div id=\"tags_title_wrapper\">")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div id=\"tags_title\">Tags</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div id=\"tags_content\">")) == -1) goto err;
if (t->tag_count == 0) {
if ((tp_ret = tp->tp_puts(tp, "<div id=\"err_content\">")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "This repository contains no tags")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
} else {
TAILQ_FOREACH(rt, &t->repo_tags, entry) {
#line 608 "../gotwebd/pages.tmpl"
if (commit_found ||!strcmp(qs->commit, rt->commit_id)) {
commit_found = 1; 
if ((tp_ret = tag_item(tp, rt)) == -1) goto err;
}
}
#line 613 "../gotwebd/pages.tmpl"
if (t->next_id ||t->prev_id) {
qs->action = TAGS; 
if ((tp_ret = gotweb_render_navs(tp)) == -1) goto err;
}
}
#line 618 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
err:
return tp_ret;
}
#line 621 "../gotwebd/pages.tmpl"
int
tag_item(struct template *tp, struct repo_tag *rt)
{
int tp_ret = 0;
#line 623 "../gotwebd/pages.tmpl"
struct request *c = tp->tp_arg;
struct transport *t = c->t;
struct repo_dir *repo_dir = t->repo_dir;
char *tag_name = rt->tag_name;
char *msg = rt->tag_commit;
char *nl;
struct gotweb_url url = {
.action = TAG,
.index_page = -1,
.page = -1,
.path = repo_dir->name,
.commit = rt->commit_id,
};
#line 637 "../gotwebd/pages.tmpl"
if (strncmp(tag_name, "refs/tags/", 10) == 0)
tag_name += 10;
#line 640 "../gotwebd/pages.tmpl"
if (msg) {
nl = strchr(msg, '\n');
if (nl)
*nl = '\0';
}
#line 646 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "<div class=\"tag_age\">")) == -1) goto err;
if ((tp_ret = gotweb_render_age(tp, rt->tagger_time, TM_DIFF)) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div class=\"tag\">")) == -1) goto err;
#line 649 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_escape(tp, tag_name)) == -1)
goto err;
#line 649 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div class=\"tag_log\">")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<a href=\"")) == -1) goto err;
#line 651 "../gotwebd/pages.tmpl"
if ((tp_ret = gotweb_render_url(c, &url)) == -1) goto err;
#line 651 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "\">")) == -1) goto err;
if ((tp_ret = tp->tp_escape(tp, msg)) == -1)
goto err;
if ((tp_ret = tp->tp_puts(tp, "</a>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div class=\"navs_wrapper\">")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div class=\"navs\">")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<a href=\"")) == -1) goto err;
#line 657 "../gotwebd/pages.tmpl"
if ((tp_ret = gotweb_render_url(c, &url)) == -1) goto err;
#line 657 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "\">tag</a>")) == -1) goto err;
if ((tp_ret = tp->tp_escape(tp, " | ")) == -1)
goto err;
url.action = BRIEFS; 
if ((tp_ret = tp->tp_puts(tp, "<a href=\"")) == -1) goto err;
#line 660 "../gotwebd/pages.tmpl"
if ((tp_ret = gotweb_render_url(c, &url)) == -1) goto err;
#line 660 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "\">commit briefs</a>")) == -1) goto err;
if ((tp_ret = tp->tp_escape(tp, " | ")) == -1)
goto err;
url.action = COMMITS; 
if ((tp_ret = tp->tp_puts(tp, "<a href=\"")) == -1) goto err;
#line 663 "../gotwebd/pages.tmpl"
if ((tp_ret = gotweb_render_url(c, &url)) == -1) goto err;
#line 663 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "\">commits</a>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div class=\"dotted_line\"></div>")) == -1) goto err;
err:
return tp_ret;
}
#line 669 "../gotwebd/pages.tmpl"
int
gotweb_render_tag(struct template *tp)
{
int tp_ret = 0;
#line 671 "../gotwebd/pages.tmpl"
struct request *c = tp->tp_arg;
struct transport *t = c->t;
struct repo_tag *rt;
const char *tag_name;
#line 676 "../gotwebd/pages.tmpl"
rt = TAILQ_LAST(&t->repo_tags, repo_tags_head);
tag_name = rt->tag_name;
#line 679 "../gotwebd/pages.tmpl"
if (strncmp(tag_name, "refs/", 5) == 0)
tag_name += 5;
#line 682 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "<div id=\"tags_title_wrapper\">")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div id=\"tags_title\">Tag</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div id=\"tags_content\">")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div id=\"tag_header_wrapper\">")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div id=\"tag_header\">")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div class=\"header_commit_title\">Commit:</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div class=\"header_commit\">")) == -1) goto err;
if ((tp_ret = tp->tp_escape(tp, rt->commit_id)) == -1)
goto err;
if ((tp_ret = tp->tp_escape(tp, " ")) == -1)
goto err;
if ((tp_ret = tp->tp_puts(tp, "<span class=\"refs_str\">(")) == -1) goto err;
#line 692 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_escape(tp, tag_name)) == -1)
goto err;
#line 692 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, ")</span>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div class=\"header_author_title\">Tagger:</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div class=\"header_author\">")) == -1) goto err;
#line 695 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_escape(tp, rt->tagger)) == -1)
goto err;
#line 695 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div class=\"header_age_title\">Date:</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div class=\"header_age\">")) == -1) goto err;
if ((tp_ret = gotweb_render_age(tp, rt->tagger_time, TM_LONG)) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div id=\"header_commit_msg_title\">Message:</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div id=\"header_commit_msg\">")) == -1) goto err;
#line 701 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_escape(tp, rt->commit_msg)) == -1)
goto err;
#line 701 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div class=\"dotted_line\"></div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div id=\"tag_commit\">")) == -1) goto err;
if ((tp_ret = tp->tp_escape(tp, "\n")) == -1)
goto err;
if ((tp_ret = tp->tp_escape(tp, rt->tag_commit)) == -1)
goto err;
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
err:
return tp_ret;
}
#line 712 "../gotwebd/pages.tmpl"
int
gotweb_render_diff(struct template *tp, FILE *fp)
{
int tp_ret = 0;
#line 714 "../gotwebd/pages.tmpl"
struct request *c = tp->tp_arg;
struct transport *t = c->t;
struct repo_commit *rc = TAILQ_FIRST(&t->repo_commits);
char *line = NULL;
size_t linesize = 0;
ssize_t linelen;
#line 721 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "<div id=\"diff_title_wrapper\">")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div id=\"diff_title\">Commit Diff</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div id=\"diff_content\">")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div id=\"diff_header_wrapper\">")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div id=\"diff_header\">")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div class=\"header_commit_title\">Commit:</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div class=\"header_commit\">")) == -1) goto err;
#line 728 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_escape(tp, rc->commit_id)) == -1)
goto err;
#line 728 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div class=\"header_author_title\">From:</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div class=\"header_author\">")) == -1) goto err;
#line 730 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_escape(tp, rc->author)) == -1)
goto err;
#line 730 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
if (strcmp(rc->committer, rc->author) != 0) {
if ((tp_ret = tp->tp_puts(tp, "<div class=\"header_author_title\">Via:</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div class=\"header_author\">")) == -1) goto err;
#line 733 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_escape(tp, rc->committer)) == -1)
goto err;
#line 733 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
}
#line 735 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "<div class=\"header_age_title\">Date:</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div class=\"header_age\">")) == -1) goto err;
if ((tp_ret = gotweb_render_age(tp, rc->committer_time, TM_LONG)) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div id=\"header_commit_msg_title\">Message:</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div id=\"header_commit_msg\">")) == -1) goto err;
#line 740 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_escape(tp, rc->commit_msg)) == -1)
goto err;
#line 740 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div class=\"dotted_line\"></div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div id=\"diff\">")) == -1) goto err;
if ((tp_ret = tp->tp_escape(tp, "\n")) == -1)
goto err;
while ((linelen = getline(&line, &linesize, fp)) != -1) {
#line 747 "../gotwebd/pages.tmpl"
if ((tp_ret = diff_line(tp, line)) == -1) goto err;
}
#line 749 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
err:
free(line); 
return tp_ret;
}
#line 755 "../gotwebd/pages.tmpl"
int
diff_line(struct template *tp, char *line )
{
int tp_ret = 0;
#line 757 "../gotwebd/pages.tmpl"
const char *color = NULL;
char *nl;
#line 760 "../gotwebd/pages.tmpl"
if (!strncmp(line, "-", 1))
color = "diff_minus";
else if (!strncmp(line, "+", 1))
color = "diff_plus";
else if (!strncmp(line, "@@", 2))
color = "diff_chunk_header";
else if (!strncmp(line, "commit +", 8) ||
!strncmp(line, "commit -", 8) ||
!strncmp(line, "blob +", 6) ||
!strncmp(line, "blob -", 6) ||
!strncmp(line, "file +", 6) ||
!strncmp(line, "file -", 6))
color = "diff_meta";
else if (!strncmp(line, "from:", 5) || !strncmp(line, "via:", 4))
color = "diff_author";
else if (!strncmp(line, "date:", 5))
color = "diff_date";
#line 778 "../gotwebd/pages.tmpl"
nl = strchr(line, '\n');
if (nl)
*nl = '\0';
#line 782 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "<div class=\"diff_line ")) == -1) goto err;
#line 782 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_escape(tp, color)) == -1)
goto err;
#line 782 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "\">")) == -1) goto err;
#line 782 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_escape(tp, line)) == -1)
goto err;
#line 782 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
err:
return tp_ret;
}
#line 786 "../gotwebd/pages.tmpl"
int
gotweb_render_branches(struct template *tp, struct got_reflist_head *refs)
{
int tp_ret = 0;
#line 788 "../gotwebd/pages.tmpl"
struct got_reflist_entry *re;
#line 790 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "<div id=\"branches_title_wrapper\">")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div id=\"branches_title\">Branches</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div id=\"branches_content\">")) == -1) goto err;
TAILQ_FOREACH(re, refs, entry) {
#line 795 "../gotwebd/pages.tmpl"
if (!got_ref_is_symbolic(re->ref)) {
if ((tp_ret = branch(tp, re)) == -1) goto err;
}
}
#line 799 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
err:
return tp_ret;
}
#line 802 "../gotwebd/pages.tmpl"
int
branch(struct template *tp, struct got_reflist_entry *re)
{
int tp_ret = 0;
#line 804 "../gotwebd/pages.tmpl"
const struct got_error *err;
struct request *c = tp->tp_arg;
struct querystring *qs = c->t->qs;
const char *refname;
time_t age;
struct gotweb_url url = {
.action = SUMMARY,
.index_page = -1,
.page = -1,
.path = qs->path,
};
#line 816 "../gotwebd/pages.tmpl"
refname = got_ref_get_name(re->ref);
#line 818 "../gotwebd/pages.tmpl"
err = got_get_repo_age(&age, c, refname);
if (err) {
log_warnx("%s: %s", __func__, err->msg);
return -1;
}
#line 824 "../gotwebd/pages.tmpl"
if (strncmp(refname, "refs/heads/", 11) == 0)
refname += 11;
#line 827 "../gotwebd/pages.tmpl"
url.headref = refname;
#line 829 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "<div class=\"branches_wrapper\">")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div class=\"branches_age\">")) == -1) goto err;
if ((tp_ret = gotweb_render_age(tp, age, TM_DIFF)) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div class=\"branches_space\">&nbsp;</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div class=\"branch\">")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<a href=\"")) == -1) goto err;
#line 835 "../gotwebd/pages.tmpl"
if ((tp_ret = gotweb_render_url(c, &url)) == -1) goto err;
#line 835 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "\">")) == -1) goto err;
#line 835 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_escape(tp, refname)) == -1)
goto err;
#line 835 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "</a>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div class=\"navs_wrapper\">")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div class=\"navs\">")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<a href=\"")) == -1) goto err;
#line 839 "../gotwebd/pages.tmpl"
if ((tp_ret = gotweb_render_url(c, &url)) == -1) goto err;
#line 839 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "\">summary</a>")) == -1) goto err;
if ((tp_ret = tp->tp_escape(tp, " | ")) == -1)
goto err;
url.action = BRIEFS; 
if ((tp_ret = tp->tp_puts(tp, "<a href=\"")) == -1) goto err;
#line 842 "../gotwebd/pages.tmpl"
if ((tp_ret = gotweb_render_url(c, &url)) == -1) goto err;
#line 842 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "\">commit briefs</a>")) == -1) goto err;
if ((tp_ret = tp->tp_escape(tp, " | ")) == -1)
goto err;
url.action = COMMITS; 
if ((tp_ret = tp->tp_puts(tp, "<a href=\"")) == -1) goto err;
#line 845 "../gotwebd/pages.tmpl"
if ((tp_ret = gotweb_render_url(c, &url)) == -1) goto err;
#line 845 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "\">commits</a>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div class=\"dotted_line\"></div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
err:
return tp_ret;
}
#line 853 "../gotwebd/pages.tmpl"
int
gotweb_render_summary(struct template *tp, struct got_reflist_head *refs)
{
int tp_ret = 0;
#line 855 "../gotwebd/pages.tmpl"
struct request *c = tp->tp_arg;
struct server *srv = c->srv;
struct transport *t = c->t;
#line 859 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "<div id=\"summary_wrapper\">")) == -1) goto err;
if (srv->show_repo_description) {
if ((tp_ret = tp->tp_puts(tp, "<div id=\"description_title\">Description:</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div id=\"description\">")) == -1) goto err;
#line 862 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_escape(tp, t->repo_dir->description)) == -1)
goto err;
#line 862 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
}
#line 864 "../gotwebd/pages.tmpl"
if (srv->show_repo_owner) {
if ((tp_ret = tp->tp_puts(tp, "<div id=\"repo_owner_title\">Owner:</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div id=\"repo_owner\">")) == -1) goto err;
#line 866 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_escape(tp, t->repo_dir->owner)) == -1)
goto err;
#line 866 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
}
#line 868 "../gotwebd/pages.tmpl"
if (srv->show_repo_age) {
if ((tp_ret = tp->tp_puts(tp, "<div id=\"last_change_title\">Last Change:</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div id=\"last_change\">")) == -1) goto err;
if ((tp_ret = gotweb_render_age(tp, t->repo_dir->age, TM_DIFF)) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
}
#line 874 "../gotwebd/pages.tmpl"
if (srv->show_repo_cloneurl) {
if ((tp_ret = tp->tp_puts(tp, "<div id=\"cloneurl_title\">Clone URL:</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div id=\"cloneurl\">")) == -1) goto err;
#line 876 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_escape(tp, t->repo_dir->url)) == -1)
goto err;
#line 876 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
}
#line 878 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
if ((tp_ret = gotweb_render_briefs(tp)) == -1) goto err;
if ((tp_ret = gotweb_render_tags(tp)) == -1) goto err;
if ((tp_ret = gotweb_render_branches(tp, refs)) == -1) goto err;
err:
return tp_ret;
}
#line 884 "../gotwebd/pages.tmpl"
int
gotweb_render_blame(struct template *tp)
{
int tp_ret = 0;
#line 886 "../gotwebd/pages.tmpl"
const struct got_error *err;
struct request *c = tp->tp_arg;
struct transport *t = c->t;
struct repo_commit *rc = TAILQ_FIRST(&t->repo_commits);
#line 891 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "<div id=\"blame_title_wrapper\">")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div id=\"blame_title\">Blame</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div id=\"blame_content\">")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div id=\"blame_header_wrapper\">")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div id=\"blame_header\">")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div class=\"header_age_title\">Date:</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div class=\"header_age\">")) == -1) goto err;
if ((tp_ret = gotweb_render_age(tp, rc->committer_time, TM_LONG)) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div id=\"header_commit_msg_title\">Message:</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div id=\"header_commit_msg\">")) == -1) goto err;
#line 902 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_escape(tp, rc->commit_msg)) == -1)
goto err;
#line 902 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div class=\"dotted_line\"></div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div id=\"blame\">")) == -1) goto err;
if ((tp_ret = tp->tp_escape(tp, "\n")) == -1)
goto err;
#line 909 "../gotwebd/pages.tmpl"
err = got_output_file_blame(c, &blame_line);
if (err) {
log_warnx("%s: got_output_file_blame: %s", __func__,
err->msg);
return (-1);
}
#line 916 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
err:
return tp_ret;
}
#line 921 "../gotwebd/pages.tmpl"
int
blame_line(struct template *tp, const char *line, struct blame_line *bline, int lprec, int lcur)
{
int tp_ret = 0;
#line 923 "../gotwebd/pages.tmpl"
struct request *c = tp->tp_arg;
struct transport *t = c->t;
struct repo_dir *repo_dir = t->repo_dir;
char *committer, *s;
struct gotweb_url url = {
.action = DIFF,
.index_page = -1,
.page = -1,
.path = repo_dir->name,
.commit = bline->id_str,
};
#line 935 "../gotwebd/pages.tmpl"
s = strchr(bline->committer, '<');
committer = s ? s + 1 : bline->committer;
#line 938 "../gotwebd/pages.tmpl"
s = strchr(committer, '@');
if (s)
*s = '\0';
#line 942 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "<div class=\"blame_wrapper\">")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div class=\"blame_number\">")) == -1) goto err;
#line 943 "../gotwebd/pages.tmpl"
if (asprintf(&tp->tp_tmp,  "%.*d", lprec, lcur) == -1)
goto err;
if ((tp_ret = tp->tp_escape(tp, tp->tp_tmp)) == -1)
goto err;
free(tp->tp_tmp);
tp->tp_tmp = NULL;
#line 943 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div class=\"blame_hash\">")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<a href=\"")) == -1) goto err;
#line 945 "../gotwebd/pages.tmpl"
if ((tp_ret = gotweb_render_url(c, &url)) == -1) goto err;
#line 945 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "\">")) == -1) goto err;
if (asprintf(&tp->tp_tmp,  "%.8s", bline->id_str) == -1)
goto err;
if ((tp_ret = tp->tp_escape(tp, tp->tp_tmp)) == -1)
goto err;
free(tp->tp_tmp);
tp->tp_tmp = NULL;
if ((tp_ret = tp->tp_puts(tp, "</a>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div class=\"blame_date\">")) == -1) goto err;
#line 949 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_escape(tp, bline->datebuf)) == -1)
goto err;
#line 949 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div class=\"blame_author\">")) == -1) goto err;
#line 950 "../gotwebd/pages.tmpl"
if (asprintf(&tp->tp_tmp,  "%.9s", committer) == -1)
goto err;
if ((tp_ret = tp->tp_escape(tp, tp->tp_tmp)) == -1)
goto err;
free(tp->tp_tmp);
tp->tp_tmp = NULL;
#line 950 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<div class=\"blame_code\">")) == -1) goto err;
#line 951 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_escape(tp, line)) == -1)
goto err;
#line 951 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</div>")) == -1) goto err;
err:
return tp_ret;
}
#line 955 "../gotwebd/pages.tmpl"
int
gotweb_render_rss(struct template *tp)
{
int tp_ret = 0;
#line 957 "../gotwebd/pages.tmpl"
struct request *c = tp->tp_arg;
struct server *srv = c->srv;
struct transport *t = c->t;
struct repo_dir *repo_dir = t->repo_dir;
struct repo_tag *rt;
struct gotweb_url summary = {
.action = SUMMARY,
.index_page = -1,
.page = -1,
.path = repo_dir->name,
};
#line 969 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<rss version=\"2.0\" xmlns:content=\"http://purl.org/rss/1.0/modules/content/\">")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<channel>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<title>Tags of ")) == -1) goto err;
#line 972 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_escape(tp, repo_dir->name)) == -1)
goto err;
#line 972 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "</title>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<link>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<![CDATA[")) == -1) goto err;
if ((tp_ret = gotweb_render_absolute_url(c, &summary)) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "]]>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</link>")) == -1) goto err;
if (srv->show_repo_description) {
if ((tp_ret = tp->tp_puts(tp, "<description>")) == -1) goto err;
#line 979 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_escape(tp, repo_dir->description)) == -1)
goto err;
#line 979 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "</description>")) == -1) goto err;
}
TAILQ_FOREACH(rt, &t->repo_tags, entry) {
#line 982 "../gotwebd/pages.tmpl"
if ((tp_ret = rss_tag_item(tp, rt)) == -1) goto err;
}
#line 984 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "</channel>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</rss>")) == -1) goto err;
err:
return tp_ret;
}
#line 988 "../gotwebd/pages.tmpl"
int
rss_tag_item(struct template *tp, struct repo_tag *rt)
{
int tp_ret = 0;
#line 990 "../gotwebd/pages.tmpl"
struct request *c = tp->tp_arg;
struct transport *t = c->t;
struct repo_dir *repo_dir = t->repo_dir;
char *tag_name = rt->tag_name;
struct gotweb_url tag = {
.action = TAG,
.index_page = -1,
.page = -1,
.path = repo_dir->name,
.commit = rt->commit_id,
};
#line 1002 "../gotwebd/pages.tmpl"
if (strncmp(tag_name, "refs/tags/", 10) == 0)
tag_name += 10;
#line 1005 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "<item>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<title>")) == -1) goto err;
#line 1006 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_escape(tp, repo_dir->name)) == -1)
goto err;
#line 1006 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_escape(tp, " ")) == -1)
goto err;
#line 1006 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_escape(tp, tag_name)) == -1)
goto err;
#line 1006 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "</title>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<link>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<![CDATA[")) == -1) goto err;
if ((tp_ret = gotweb_render_absolute_url(c, &tag)) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "]]>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</link>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<description>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<![CDATA[<pre>")) == -1) goto err;
#line 1013 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_escape(tp, rt->tag_commit)) == -1)
goto err;
#line 1013 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "</pre>]]>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</description>")) == -1) goto err;
if ((tp_ret = rss_author(tp, rt->tagger)) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<guid isPermaLink=\"false\">")) == -1) goto err;
#line 1016 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_escape(tp, rt->commit_id)) == -1)
goto err;
#line 1016 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "</guid>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "<pubDate>")) == -1) goto err;
if ((tp_ret = gotweb_render_age(tp, rt->tagger_time, TM_RFC822)) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</pubDate>")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</item>")) == -1) goto err;
err:
return tp_ret;
}
#line 1023 "../gotwebd/pages.tmpl"
int
rss_author(struct template *tp, char *author)
{
int tp_ret = 0;
#line 1025 "../gotwebd/pages.tmpl"
char *t, *mail;
#line 1027 "../gotwebd/pages.tmpl"
/* what to do if the author name contains a paren? */
if (strchr(author, '(') != NULL || strchr(author, ')') != NULL)
return 0;
#line 1031 "../gotwebd/pages.tmpl"
t = strchr(author, '<');
if (t == NULL)
return 0;
*t = '\0';
mail = t+1;
#line 1037 "../gotwebd/pages.tmpl"
while (isspace((unsigned char)*--t))
*t = '\0';
#line 1040 "../gotwebd/pages.tmpl"
t = strchr(mail, '>');
if (t == NULL)
return 0;
*t = '\0';
#line 1045 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "<author>")) == -1) goto err;
if ((tp_ret = tp->tp_escape(tp, mail)) == -1)
goto err;
#line 1046 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_escape(tp, " ")) == -1)
goto err;
#line 1046 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, "(")) == -1) goto err;
#line 1046 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_escape(tp, author)) == -1)
goto err;
#line 1046 "../gotwebd/pages.tmpl"
if ((tp_ret = tp->tp_puts(tp, ")")) == -1) goto err;
if ((tp_ret = tp->tp_puts(tp, "</author>")) == -1) goto err;
err:
return tp_ret;
}
